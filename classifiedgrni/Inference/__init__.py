"""
This submodule contains different classification-based data-driven scoring
functions to infer GRNs from gene expression datasets
"""
__author__ = "Sergio Peignier and Pauline Schmitt"
__copyright__ = "Copyright 2019, The GReNaDIne Project"
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Sergio Peignier"
__email__ = "sergio.peignier@insa-lyon.fr"
__status__ = "pre-alpha"
