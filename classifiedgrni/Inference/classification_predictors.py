"""
GRN Co-Expression Inference with classification methods
---------------------------------------------------------

This module allows to infer Gene Regulatory Networks using
gene expresion data (RNAseq or Microarray). This module implements several
inference algorithms based on classification, using `scikit-learn`_.

.. _scikit-learn:
    https://scikit-learn.org
"""
__author__ = "Pauline Schmitt, Sergio Peignier"
__copyright__ = "Copyright 2019, The GReNaDIne Project"
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Sergio Peignier"
__email__ = "sergio.peignier@insa-lyon.fr"
__status__ = "pre-alpha"

import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.svm import SVC
from collections import Counter

def RF_classifier_score(X, y, **rf_parameters):
    """
    Random Forest Classifier, score predictor function based on `scikit-learn`_
    RandomForestClassifier.

    Args:
        X (pandas.DataFrame): Transcription factor gene expressions (discretized
            or not) where rows are experimental conditions and columns are
            transcription factors
        y (pandas.Series): Target gene expression vector (discretized) where
            rows are experimental conditions
        **rf_parameters: Named parameters for the sklearn RandomForestClassifier

    Returns:
        numpy.array: co-regulation scores.

        The i-th element of the score array represents the score assigned by the
        RandomForestClassifier to the regulatory relationship between the target
        gene and transcription factor i.

    Examples:
        >>> import pandas as pd
        >>> import numpy as np
        >>> np.random.seed(0)
        >>> tfs = pd.DataFrame(np.random.randn(5,3),
                               index =["c1","c2","c3","c4","c5"],
                               columns=["tf1","tf2","tf3"])
        >>> tg = pd.Series(np.random.randint(0,3,size=5), index=["c1","c2","c3","c4","c5"])
        >>> scores = RF_classifier_score(tfs,tg)
        >>> scores
        array([0.21071429, 0.4       , 0.28928571])
    """
    classifier = RandomForestClassifier(**rf_parameters)
    classifier.fit(X, y)
    scores = classifier.feature_importances_
    return scores

def XRF_classifier_score(X, y, **xrf_parameters):
    """
    Randomized decision trees Classifier, score predictor function based on
    `scikit-learn`_ ExtraTreesClassifier.

    Args:
        X (pandas.DataFrame): Transcription factor gene expressions (discretized
            or not) where rows are experimental conditions and columns are
            transcription factors
        y (pandas.Series): Target gene expression vector (discretized) where
            rows are experimental conditions
        **xrf_parameters: Named parameters for the sklearn ExtraTreesClassifier

    Returns:
        numpy.array: co-regulation scores.

        The i-th element of the score array represents the score assigned by the
        ExtraTreesClassifier to the regulatory relationship between the target
        gene and transcription factor i.

    Examples:
        >>> import pandas as pd
        >>> import numpy as np
        >>> np.random.seed(0)
        >>> tfs = pd.DataFrame(np.random.randn(5,3),
                               index =["c1","c2","c3","c4","c5"],
                               columns=["tf1","tf2","tf3"])
        >>> tg = pd.Series(np.random.randint(0,3,size=5), index=["c1","c2","c3","c4","c5"])
        >>> scores = XRF_classifier_score(tfs,tg)
        >>> scores
        array([0.31354167, 0.35520833, 0.33125   ])
    """
    classifier = ExtraTreesClassifier(**xrf_parameters)
    classifier.fit(X, y)
    scores = classifier.feature_importances_
    return(scores)

def AdaBoost_classifier_score(X, y, **adab_parameters):
    """
    AdaBoost Classifier, score predictor function based on `scikit-learn`_
    AdaBoostClassifier.

    Args:
        X (pandas.DataFrame): Transcription factor gene expressions (discretized
            or not) where rows are experimental conditions and columns are
            transcription factors
        y (pandas.Series): Target gene expression vector (discretized) where
            rows are experimental conditions
        **adab_parameters: Named parameters for the sklearn AdaBoostClassifier

    Returns:
        numpy.array: co-regulation scores.

        The i-th element of the score array represents the score assigned by the
        AdaBoostClassifier to the regulatory relationship between the target
        gene and transcription factor i.

    Examples:
        >>> import pandas as pd
        >>> import numpy as np
        >>> np.random.seed(0)
        >>> tfs = pd.DataFrame(np.random.randn(5,3),
                               index =["c1","c2","c3","c4","c5"],
                               columns=["tf1","tf2","tf3"])
        >>> tg = pd.Series(np.random.randint(0,3,size=5), index=["c1","c2","c3","c4","c5"])
        >>> scores = AdaBoost_classifier_score(tfs,tg)
        >>> scores
        array([0.24, 0.44, 0.32])
    """
    classifier = AdaBoostClassifier(**adab_parameters)
    classifier.fit(X, y)
    scores = classifier.feature_importances_
    return scores

def GB_classifier_score(X, y, **gb_parameters):
    """
    Gradient Boosting Classifier, score predictor function based on
    `scikit-learn`_ GradientBoostingClassifier.

    Args:
        X (pandas.DataFrame): Transcription factor gene expressions (discretized
            or not) where rows are experimental conditions and columns are
            transcription factors
        y (pandas.Series): Target gene expression vector (discretized) where
            rows are experimental conditions
        **gb_parameters: Named parameters for the sklearn ExtraTreesClassifier

    Returns:
        numpy.array: co-regulation scores.

        The i-th element of the score array represents the score assigned by the
        GradientBoostingClassifier to the regulatory relationship between the target
        gene and transcription factor i.

    Examples:
        >>> import pandas as pd
        >>> import numpy as np
        >>> np.random.seed(0)
        >>> tfs = pd.DataFrame(np.random.randn(5,3),
                               index =["c1","c2","c3","c4","c5"],
                               columns=["tf1","tf2","tf3"])
        >>> tg = pd.Series(np.random.randint(0,3,size=5), index=["c1","c2","c3","c4","c5"])
        >>> scores = GB_classifier_score(tfs,tg)
        >>> scores
         array([0.33959125, 0.21147015, 0.4489386 ])
    """
    classifier = GradientBoostingClassifier(**gb_parameters)
    classifier.fit(X, y)
    scores = classifier.feature_importances_
    return scores

def SVM_classifier_score(X, y, **svm_parameters):
    """
    SVM Classifier, score predictor function based on `scikit-learn`_ SVC
    (Support Vector Classifier).

    Args:
        X (pandas.DataFrame): Transcription factor gene expressions (discretized
            or not) where rows are experimental conditions and columns are
            transcription factors
        y (pandas.Series): Target gene expression vector (discretized) where
            rows are experimental conditions
        **svm_parameters: Named parameters for the sklearn SVC

    Returns:
        numpy.array: co-regulation scores.

        The i-th element of the score array represents the score assigned by the
        SVC to the regulatory relationship between the target
        gene and transcription factor i.

    Examples:
        >>> import pandas as pd
        >>> import numpy as np
        >>> np.random.seed(0)
        >>> tfs = pd.DataFrame(np.random.randn(5,3),
                               index =["c1","c2","c3","c4","c5"],
                               columns=["tf1","tf2","tf3"])
        >>> tg = pd.Series(np.random.randint(0,3,size=5), index=["c1","c2","c3","c4","c5"])
        >>> scores = SVM_classifier_score(tfs,tg)
        >>> scores
        array([0.58413783, 0.5448345 , 0.31764191])
    """
    # consider changing kernel function: SVC(kernel = 'linear'/'poly'/'sigmoid'/'precomputed')
    # default is rbf
    # also consider changing polynomial degree : SVC(kernel = 'poly', degree = int value)
    # default is 3
    # but linear is the only kernel that has coef_ attribute
    svm_parameters["kernel"] = 'linear'
    svm_parameters["decision_function_shape"] = 'ovr'
    classifier = SVC(**svm_parameters)
    classifier.fit(X, y)
    # coef_ = array of shape (nb_classes, nb_TFs)
    # replacing each class value (= discretized value) of y with corresponding TFs weights from attribute coef_
    # then taking the mean along each column (= mean importance of TF over all conditions)
    if classifier.coef_.shape[0] > 1:
        scores = np.abs(classifier.coef_).mean(axis=0)
        #coef = pd.DataFrame(classifier.coef_,index=classifier.classes_)
        #nb_classes = pd.Series(Counter(y))
        #scores = np.abs((coef.T*nb_classes/nb_classes.sum()).T).mean(axis=0)
    else:
        scores = np.abs(classifier.coef_[0,:])
    return scores
