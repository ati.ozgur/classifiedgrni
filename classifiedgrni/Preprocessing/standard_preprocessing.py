# -*- coding: utf-8 -*-
"""
Gene Expression pre-processing
------------------------------

This module allows to pre-process gene expression data.

"""
import numpy as np
import pandas as pd
import ot

__author__ = "Sergio Peignier"
__copyright__ = "Copyright 2019, The GReNaDIne Project"
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Sergio Peignier"
__email__ = "sergio.peignier@insa-lyon.fr"
__status__ = "pre-alpha"

def z_score(A, axis=0):
    """
    Compute the z-score along the specified axis.

    Args:
        A (pandas.DataFrame or numpy.array): matrix
        axis (int): 0 for columns and 1 for rows

    Returns:
        pandas.DataFrame or numpy.array: Normalized matrix

    Examples:
        >>> import pandas as pd
        >>> import numpy as np
        >>> np.random.seed(0)
        >>> data = pd.DataFrame(np.random.randn(5, 5),
                            index=["c1", "c2", "c3", "c4", "c5"],
                            columns=["gene1", "gene2", "gene3", "gene4", "gene5"])
        >>> norm_data = z_score(data)
        >>> norm_data
               gene1     gene2     gene3     gene4     gene5
        c1  1.254757 -1.222682  0.914682  1.672581  0.828015
        c2 -0.446591 -0.083589 -1.038607 -0.418644 -0.331945
        c3  0.249333  0.960749  0.538403 -0.218012 -0.305461
        c4  0.367024  1.043200 -1.131598 -0.047267 -1.338834
        c5 -1.424523 -0.697678  0.717120 -0.988659  1.148225

    """
    if axis==0:
        A = (A - A.mean()) / A.std()
    if axis==1:
        A = A.T
        A = (A - A.mean()) / A.std()
        A = A.T
    return(A)

def mean_std_polishing(A, nb_iterations=5):
    """
    Iterative z-score on rows and columns.

    Args:
        A (pandas.DataFrame or numpy.array): matrix
        nb_iterations (int): number of polishing iterations

    Returns:
        pandas.DataFrame or numpy.array: Polished matrix

    Examples:
        >>> import pandas as pd
        >>> import numpy as np
        >>> np.random.seed(0)
        >>> data = pd.DataFrame(np.random.randn(5, 5),
                            index=["c1", "c2", "c3", "c4", "c5"],
                            columns=["gene1", "gene2", "gene3", "gene4", "gene5"])
        >>> norm_data = mean_std_polishing(data)
        >>> norm_data
               gene1     gene2     gene3     gene4     gene5
        c1  0.336095 -1.618781  0.187436  1.109617 -0.014367
        c2 -0.321684  0.586608 -1.606905  0.484159  0.857821
        c3  0.139260  0.860934  0.976541 -1.395814 -0.580921
        c4  1.243263  0.421752 -0.585940  0.282319 -1.361394
        c5 -1.363323 -0.161066  0.826375 -0.421998  1.120013

    """
    for i in range(nb_iterations):
        A = A-A.mean(axis=0)
        # std polish the column
        A = A/A.std(axis=0)
        # mean polish the row
        A = (A.T-A.T.mean(axis=0)).T
        # std polish the row
        A = (A.T/A.T.std(axis=0)).T
    return(A)
